# grow.update-mcm.cmd.sh
#
# by Keith Hodges 2010
#
# update-mcm

command="update-mcm"
description="update-mcm"
usage="usage:
$scriptName update-mcm <directory>
$scriptName update-mcm <directory> --url <url>
$scriptName update-mcm <directory> --push -m <msg> #push to repo

Obtains the Monticello Configuration file, downloads the packages referenced, and optionally pushes this back up to the repository.
"

if $SHOWHELP; then
	echo "$command - $description\n\n$usage"
fi
if $METADATAONLY; then
	return
fi

push=false
url=""
signalUrl=false
message="Auto Commit"
signalMessage=false

for arg in $@
do
	case $arg in
	  --url)
	  		#signal to pick up next argument
	    	signalUrl=true
	    ;;
	  --push)
	    	push=true
	    ;;
	  -m)
	  		#signal to pick up next argument
	    	signalMessage=true
	    ;;
	esac
	
	#pick up the next argument
	if [[ "$signalUrl" = "true" && "$arg" != "--url" ]]; then
		url=$arg #grab argument
		signalUrl=false #reset signal
	fi
	
	if [[ "$signalMessage" = "true" && "$arg" != "-m" ]]; then
		message=$arg #grab argument
		signalDest=false #reset signal
	fi
done
 
directory=$1
if [[ -z "$directory" || "${directory:0:1}" = "-" ]]; then
	echo "Please supply a directory\n\n$usage"
	exit 1
fi

list=`find "$directory" -name "*.mcm" -type f`

IFS=$'\n'
for mcm in $list
do
 	url="${mcm##*/-}"
	url="${url//\://}"
	url="${url// /%20}"
	url="${url/-/:}"

  	if $DEBUG; then
		echo "url: $url\nmcm: $mcm"
		echo "rm -rf ${mcm%/-*}/*.mcz"
	fi
		
	curl -s "$url" > "$mcm"	
		
	ruby -p -i -e 'gsub(/\r/,"\n")' "$mcm"
	#perl -pi -e 's/\r/\n/g' $found"	

	unset repos
	exec<$mcm
	count=0
	while read line
	do
		if [ "${line:0:10}" = "repository" ]; then
			repo="${line#*\'}"
			repo="${repo%%\'*}"
			repos[$count]="$repo"
			count=$((count + 1))
		fi
	done

	unset deps
	exec<$mcm
	count=0
	while read line
	do
		if [ "${line:0:10}" = "dependency" ]; then
			dep="${line#*\'*\' \'}"
			dep="${dep%%\'*}"
			deps[$count]="$dep"
			count=$((count + 1))
		fi
	done
	
	rm -rf "${mcm%/-*}/"*.mcz

	count=1
	for dep in "${deps[@]}"
	do
		for repo in "${repos[@]}"
		do
			padding="0000"
			out="${mcm%/-*}/${padding:${#count}:4}$count-$dep.mcz"
			url="${dep// /%20}"
			url="${url//(/%28}"
			url="${url//)/%29}"
			url="$repo/$url.mcz"
			if $DEBUG; then
				echo "url: $url\nfile: $out"
			fi
		
 			curl -s "$url" > "$out"
			if [ ! -s "$out" ]; then
				break 1
			fi
		done
		count=$((count + 1))
	done

done

if [[ $DEBUG || 
$DRYRUN ]]; then
	echo "DONE"
fi

if $DRYRUN; then
	return
fi
 
return

if $push; then
	cd "$destDir"
	bzr commit -m "$message"
	bzr push	
fi
 
#"This Code is distributed subject to the MIT License, as in http://www.opensource.org/licenses/mit-license.php . 
#Any additional contribution submitted for incorporation into or for distribution with this file shall be presumed subject to the same license."