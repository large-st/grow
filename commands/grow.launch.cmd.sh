# grow.launch.cmd.sh
#
# by Keith Hodges 2010
#
# Launch Images

command="launch"
description="launch an image"
usage="usage:
$scriptName launch <imageName>
$scriptName launch <imageName> --bg
$scriptName launch <imageName> --latest | -l #last version number
$scriptName launch <imageName> &

launches an image using the VM selected by the current configuration. 
[ $GROW_VM_DIR or $GROW_VM_DIR2 ]
"

if $SHOWHELP; then
	echo "$command - $description\n\n$usage"
fi
if $METADATAONLY; then
	return
fi

bg=false
latest=false
for arg in $@
do
	case $arg in
	  --bg)
	    	bg=true
	    ;;
	  --latest | -l)
	    	latest=true
	    ;;
	esac
done

if $DEBUG; then
	echo "GROW_VM_DIR=$GROW_VM_DIR"
	echo "GROW_VM_DIR2=$GROW_VM_DIR2"
fi

#FIND THE VM
if $MACOSX; then
	#find the app
	if [ "${GROW_VM_DIR##*.}" = ".app" ]; then
		vm = "$GROW_VM_DIR"
 	else
		vm=`find $GROW_VM_DIR -type d -maxdepth 2 -name *.app`
	fi
	
	if [ -z "$vm" ]; then
		if [ "${GROW_VM_DIR2##*.}" = ".app" ]; then
			vm = "$GROW_VM_DIR2"
 		else
			vm=`find $GROW_VM_DIR2 -type d -maxdepth 2 -name *.app`
		fi
	fi
	vm="${vm%%.app*}.app"
	if [ "$vm" = '.app' ]; then
		vm=""
	fi
	vm=`find $vm/Contents/MacOS -type f -perm -u=x`
fi

if $WINDOWS; then
	vm=`find $GROW_VM_DIR -type f -maxdepth 2 -name *.exe`
	vm="${vm%%.exe*}.exe"
	if [ "$vm" = '.exe' ]; then
		vm=""
	fi
fi
if $UNIX; then
	vm=`find $GROW_VM_DIR -type f -maxdepth 2 -name *.sh`
	vm="${vm%%.sh*}.sh"
	if [ "$vm" = '.sh' ]; then
		vm=""
	fi
fi

if [ -z "$vm" ]; then
	echo "Unable to find VM in $GROW_VM_DIR or $GROW_VM_DIR2"
	return
fi

if [ "$NOLAUNCH" = "true" ]; then
	#whoever called us just wants us for our ability to find a vm
	return
fi
 
image=$1
if [[ -z "$image" || "${image:0:1}" = "-" ]]; then
	echo "Found: $vm"
	echo "Please supply an image name\n\n$usage"
	exit 1
fi

if $latest; then
	if [ ${image##*.} = "image" ]; then
		image=${image:0:${#image}-6}
	fi
	list=$image.*.image
	max=0
	for each in ${list[@]}
	do
		number="${each:$(( ${#image} + 1 )):$(( ${#each} - 7 - ${#image}))}"
		if [ "$number" -gt "$max" ]; then
			max=$number
		fi
	done
	image="$image.$max.image"
fi

#append the extension if it is not present
if [ ${image##*.} != "image" ]; then
 image="$image.image"
fi

if [[ $DEBUG || $DRYRUN ]]; then
	echo "Launching:"
	echo "$vm $image"
fi

if $DRYRUN; then
	return
fi

if $bg;then
	"$vm" "$image" &
else
	"$vm" "$image"
fi
 
#"This Code is distributed subject to the MIT License, as in http://www.opensource.org/licenses/mit-license.php . 
#Any additional contribution submitted for incorporation into or for distribution with this file shall be presumed subject to the same license."