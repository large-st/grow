# grow.publish-image.cmd.sh
#
# by Keith Hodges 2010
#
# Publish Images

command="publish-image"
description="publish an image"
usage="usage:
$scriptName publish-image <imageName> --bg
$scriptName publish-image <imageName> --latest | -l #last version number
$scriptName publish-image <imageName> --dest <some-dir>
$scriptName publish-image <imageName> --push -m <msg> #push to repo

publishes an image without the version number to local 'images-<imageName>',
commits and pushes.
"

if $SHOWHELP; then
	echo "$command - $description\n\n$usage"
fi
if $METADATAONLY; then
	return
fi

bg=false
latest=true #until non latest behaviour implemented
signalDest=false
destDir=""
push=false
message="Auto Commit"
signalMessage=false

for arg in $@
do
	case $arg in
 	  --latest | -l)
	    	latest=true
	    ;;
	  --bg)
	    	bg=true
	    	LOUD=false
	    	VERBOSE=false
	    	DEBUG=false
	    ;;
	  --dest)
	  		#signal to pick up next argument
	    	signalDest=true
	    ;;
	  --push)
	    	push=true
	    ;;
	  -m)
	  		#signal to pick up next argument
	    	signalMessage=true
	    ;;
	esac
	
	#pick up the next argument
	if [[ "$signalDest" = "true" && "$arg" != "--dest" ]]; then
		destDir=$arg #grab argument
		signalDest=false #reset signal
	fi
	
	if [[ "$signalMessage" = "true" && "$arg" != "-m" ]]; then
		message=$arg #grab argument
		signalDest=false #reset signal
	fi
done
 
image=$1
if [[ -z "$image" || "${image:0:1}" = "-" ]]; then
	echo "Please supply an image name\n\n$usage"
	exit 1
fi

#knock off the extension, if it is present.
if [ "${image##*.}" = "image" ]; then
		image=${image:0:${#image}-6}
fi
 
destName="${image%%.*}"
	
if $latest; then
	list=$image.*.image
	max=0
	for each in ${list[@]}
	do
		number="${each:$(( ${#image} + 1 )):$(( ${#each} - 7 - ${#image}))}"
		if [ "$number" -gt "$max" ]; then
			max=$number
		fi
	done
fi

if [ -z "$destDir" ]; then
	destDir="images-$destName"
fi

if [[ $DEBUG || $DRYRUN ]]; then
	echo "Publishing: $image.$max.image to $destDir"
fi

if $DRYRUN; then
	return
fi

cp "$image.$max.image" "$destDir/$image.image"
cp "$image.$max.changes" "$destDir/$image.changes"

if $push; then
	cd "$destDir"
	bzr commit -m "$message"
	bzr push	
fi
 
#"This Code is distributed subject to the MIT License, as in http://www.opensource.org/licenses/mit-license.php . 
#Any additional contribution submitted for incorporation into or for distribution with this file shall be presumed subject to the same license."