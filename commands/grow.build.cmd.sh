# grow.build.cmd.sh
#
# by Keith Hodges 2010
#
# A Debugging tool

command="build"
description="perform a build loading exported sources"
usage="usage:
cd <working-directory>
$scriptName build <series-directory>
"

if $SHOWHELP; then
	echo "$command - $description\n\n$usage"
fi
if $METADATAONLY; then
	return
fi

#check things are vaguely configured
if [[ -z $GROW_BASE ]]; then
	echo "Unable to find configuration"
fi

#check we have a directory
sourceDirectory=$1
if [[ -z "$sourceDirectory" || "${sourceDirectory:0:1}" = "-" ]]; then
	echo "Please supply a directory\n\n$usage"
	exit 1
fi
saveName="${sourceDirectory##*/}"
seriesMappingFile="$sourceDirectory/$GROW_SERIES"

#use launch to find the vm
NOLAUNCH="true"
source "$scriptDir/$scriptName.launch.cmd.sh"

cp -R $GROW_BASE/* $GROW_THIS
cd $GROW_BASE
image=( *.image );
image="${image[0]}"
cd $workingDir
cd $GROW_THIS

echo > start.st

for bs_list in ${GROW_BOOTSTRAP[@]}
do
	for bs in $bs_list
		do
		if $DEBUG; then
			echo "bootstrap: $bs"
		fi

		if [ ! -f "$bs" ]; then
 			echo "Not found: $bs" 
			return 1
		fi

		cat "$bs" >> start.st
		echo >> start.st
	done
done

#configure InstallSeries
#...here...
if [ -e "$seriesMappingFile" ]; then
	echo "!" >> start.st
	cat "$seriesMappingFile" >> start.st
fi

echo "\n!
InstallSeries addSource: '$sourceDirectory'." >> start.st

#show what was installed (make optional)
echo "\n!
InstallSeries pending explore containingWindow 
	setLabel: InstallSeries sources keys asArray printString.
" >> start.st

#do the install
echo "\n!
InstallSeries install.
" >> start.st

#save the image
echo "\n!
| smalltalk theName |
smalltalk := (Smalltalk respondsTo: #getSystemAttribute:)
		ifTrue: [ Smalltalk ]
		ifFalse: [ (Smalltalk at: #SmalltalkImage) current ].

theName := FileDirectory default
	nextNameFor: '$saveName' extension: 'changes'.

WorldState addDeferredUIMessage: [	
	smalltalk saveChangesInFileNamed: theName.
	smalltalk saveImageInFileNamed: ((theName copyUpToLast: $.), '.image').
].
" >> start.st

echo "\n! WorldState addDeferredUIMessage: [	
	 
]." >> start.st



if $DEBUG; then
	echo "Launching:"
	#cat start.st
	echo "$vm $image start.st"	
fi

pwd=`pwd`
if $bg;then
	"$vm" "$image" $pwd/start.st &
else
	"$vm" "$image" $pwd/start.st
fi

#"This Code is distributed subject to the MIT License, as in http://www.opensource.org/licenses/mit-license.php . 
#Any additional contribution submitted for incorporation into or for distribution with this file shall be presumed subject to the same license."